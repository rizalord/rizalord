![My card name](https://cardivo.vercel.app/api?name=Ahmad%20Khamdani&description=Junior%20Fullstack%20Developer&image=https://avatars.githubusercontent.com/u/49712569?v=4&instagram=rizalord_&linkedin=Ahmad%20Khamdani&github=rizalord&twitter=rizalord_&backgroundColor=%23FFFFFF&pattern=iLikeFood&colorPattern=%23cccccc&fontColor=%23777&iconColor=%23777&opacity=0.3)

<div align="center">
  Hi all 👋, I'm Ahmad Khamdani a 19 years old guy who likes to learn technologies, especially in the backend. I really like designing a good cloud infrastructure, testing applications, doing automation and other things that make application performance better.
  <br><br>
  <img src="https://img.shields.io/badge/typescript%20-%23007ACC.svg?&style=for-the-badge&logo=typescript&logoColor=white"/>
  <img src="https://img.shields.io/badge/nestjs-%23E0234E.svg?style=for-the-badge&logo=nestjs&logoColor=white"/>
  <img src="https://img.shields.io/badge/nuxt%20js-%2300C58E?&style=for-the-badge&logo=nuxt.js&logoColor=white"/>
  <img src="https://img.shields.io/badge/next%20js-%23000000?&style=for-the-badge&logo=nuxt.js&logoColor=white"/>
  <img src="https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&logoColor=white"/>
  <img src="https://img.shields.io/badge/Flutter-02569B?style=for-the-badge&logo=flutter&logoColor=white"/>
  <br><br>

  You can reach me here:<br><br>
  <a href="mailto:ahmadkhamdani9@gmail.com" style="text-decoration: none;">
    <img src="https://img.shields.io/badge/email%20me%20here-%23EA4335?&style=for-the-badge&logo=gmail&logoColor=white"/>
  </a>
  <a href="https://t.me/rizalord" style="text-decoration: none;">
    <img src="https://img.shields.io/badge/telegram-%2326A5E4?&style=for-the-badge&logo=telegram&logoColor=white"/>
  </a>
  <a href="http://line.me/ti/p/~rizalord" style="text-decoration: none;">
    <img src="https://img.shields.io/badge/line-%2300C300?&style=for-the-badge&logo=line&logoColor=white"/>
  </a>
  <a href="https://twitter.com/rizalord_" style="text-decoration: none;">
    <img src="https://img.shields.io/badge/twitter-%231DA1F2?&style=for-the-badge&logo=twitter&logoColor=white"/>
  </a>
  <a href="https://instagram.com/rizalord_" style="text-decoration: none;">
    <img src="https://img.shields.io/badge/instagram-%23E4405F?&style=for-the-badge&logo=instagram&logoColor=white"/>
  </a>

  Made with ♥ in Malang, Indonesia
  <br>
  <a href="https://rizalord.me" style="color: #2E3440;">rizalord.tech</a>
</div>
